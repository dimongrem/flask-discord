import os

from flask import Flask, redirect, url_for
from flask_discord import DiscordOAuth2Session, requires_authorization


app = Flask(__name__)

app.secret_key = b"%\xe0'\x01\xdeH\x8e\x85m|\xb3\xffCN\xc9g"
os.environ["OAUTHLIB_INSECURE_TRANSPORT"] = "false"    # !! Only in development environment.

app.config["DISCORD_CLIENT_ID"] = 967778629226467408
app.config["DISCORD_CLIENT_SECRET"] = 'mLoSjbbxJnr9YmDb2WYeNS3Sy0kO5yjm'
app.config["DISCORD_BOT_TOKEN"] = 'OTY3Nzc4NjI5MjI2NDY3NDA4.GTH4Oh.ugONIMIC-cUfJLIbgSaY7Xinwh5ChY132H-pdw'
app.config["DISCORD_REDIRECT_URI"] = "http://127.0.0.1:5000/callback"

discord = DiscordOAuth2Session(app)


HYPERLINK = '<a href="{}">{}</a>'


def welcome_user(user):
    dm_channel = discord.bot_request("/users/@me/channels", "POST", json={"recipient_id": user.id})
    return discord.bot_request(
        f"/channels/{dm_channel['id']}/messages", "POST", json={"content": "Thanks for authorizing the app!"}
    )


@app.route("/")
def index():
    if not discord.authorized:
        return f"""
        {HYPERLINK.format(url_for(".login"), "Login")} <br />
        {HYPERLINK.format(url_for(".login_with_data"), "Login with custom data")} <br />
        {HYPERLINK.format(url_for(".invite_bot"), "Invite Bot with permissions 8")} <br />
        {HYPERLINK.format(url_for(".invite_oauth"), "Authorize with oauth and bot invite")} <br />
        {HYPERLINK.format(url_for('.logout'), 'Logout')} <br />
        {HYPERLINK.format(url_for('.me'), 'Me')} <br />
        {HYPERLINK.format(url_for('.login_email_scope'), 'Login with EMAIL scope')} <br />
        {HYPERLINK.format(url_for('.login_guilds'), 'Login with GUILDS and GUILDS.JOIN scopes')} <br />
        {HYPERLINK.format(url_for('.login_connections'), 'Login with CONNECTIONS scope')} <br />
        {HYPERLINK.format(url_for('.login_no_prompt_bot'), 'Login without prompt as bot')} <br />
        {HYPERLINK.format(url_for('.login_no_prompt_user'), 'Login without prompt as user')} <br />
        """

    return f"""
    {HYPERLINK.format(url_for(".me"), "@ME")}<br />
    {HYPERLINK.format(url_for(".logout"), "Logout")}<br />
    {HYPERLINK.format(url_for(".user_guilds"), "My Servers")}<br />
    {HYPERLINK.format(url_for(".add_to_guild", guild_id=967795237445587005), "Add me to 215154001799413770.")}    
    """

@app.route('/login-no-prompt-user/')
def login_no_prompt_user():
    return discord.create_session(prompt=False) 

@app.route('/login-no-prompt-bot/')
def login_no_prompt_bot():
    return discord.create_session(scope=['bot'], prompt=False)

@app.route("/login/")
def login():
    return discord.create_session(scope=['identify'])

@app.route('/login-scope/')
def login_email_scope():
    return discord.create_session(scope=['email'])

@app.route('/login-guilds-join')
def login_guilds():
    return discord.create_session(scope=['guilds', 'guilds.join'])


@app.route("/login-data/")
def login_with_data():
    return discord.create_session(data=dict(redirect="/me/", coupon="15off", number=15, zero=0, status=False))

@app.route('/login-connections/')
def login_connections():
    return discord.create_session(scope=['identify', 'connections'])


@app.route("/invite-bot/")
def invite_bot():
    return discord.create_session(scope=["bot"], permissions=8, guild_id=967795237445587005, disable_guild_select=False)



@app.route("/invite-oauth/")
def invite_oauth():
    return discord.create_session(scope=["bot", "identify"], permissions=8)


@app.route("/callback/")
def callback():
    data = discord.callback()
    redirect_to = data.get("redirect", "/")

    user = discord.fetch_user()
    welcome_user(user)

    return redirect(redirect_to)


@app.route("/me/")
def me():
    user = discord.fetch_user()
    return f"""
<html>
<head>
<title>{user.name}</title>
</head>
<body><img src='{user.avatar_url or user.default_avatar_url}' />
<p>Is avatar animated: {str(user.is_avatar_animated)}</p>
<p>User email: {str(user.email)} </p>
<a href={url_for("my_connections")}>Connections</a>
<br />
</body>
</html>

"""


@app.route("/me/guilds/")
def user_guilds():
    guilds = discord.fetch_guilds()
    return "<br />".join([f"[ADMIN] {g.name}" if g.permissions.administrator else g.name for g in guilds])


@app.route("/add_to/<int:guild_id>/")
def add_to_guild(guild_id):
    user = discord.fetch_user()
    return user.add_to_guild(guild_id)


@app.route("/me/connections/")
def my_connections():
    user = discord.fetch_user()
    connections = discord.fetch_connections()
    return f"""
<html>
<head>
<title>{user.name}</title>
</head>
<body>
{str([f"{connection.name} - {connection.type}" for connection in connections])}
</body>
</html>

"""


@app.route("/logout/")
def logout():
    discord.revoke()
    return redirect(url_for(".index"))


@app.route("/secret/")
@requires_authorization
def secret():
    return os.urandom(16)





if __name__ == "__main__":
    app.run(debug=True)
