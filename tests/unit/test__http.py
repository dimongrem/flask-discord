import abc
import os
from types import SimpleNamespace

import pytest
import requests
from flask import Flask

from flask_discord import Unauthorized, RateLimited
from flask_discord._http import DiscordOAuth2HttpClient
DiscordOAuth2HttpClient.__abstractmethods__ = set()

assert isinstance(DiscordOAuth2HttpClient, abc.ABCMeta)


@pytest.fixture
def client_no_params():
    return DiscordOAuth2HttpClient()

@pytest.fixture
def client_invalid_cache():
    cache = 'cache'
    return DiscordOAuth2HttpClient(users_cache=cache)

@pytest.fixture
def client_with_proxies():
    return DiscordOAuth2HttpClient(proxy='proxy', proxy_auth='proxy_auth')



@pytest.fixture
def app(mocker):
    app = Flask(__name__)
    mocker.patch.dict(os.environ, {"OAUTHLIB_INSECURE_TRANSPORT": "true"})
    app.config['SECRET_KEY'] = b"%\xe0'\x01\xdeH\x8e\x85m|\xb3\xffCN\xc9g"
    app.config["DISCORD_CLIENT_ID"] = 490732332240863233
    app.config["DISCORD_CLIENT_SECRET"] = 'TEST_CLIENT_SECRET'
    app.config["DISCORD_BOT_TOKEN"] = 'TEST_BOT_TOKEN'
    app.config["DISCORD_REDIRECT_URI"] = "http://127.0.0.1:5000/callback"

    return app

# app init with invalid cache type -> raises ValueError
def test_init_app_invalid_cache(client_invalid_cache, app):
    with pytest.raises(ValueError):
        client_invalid_cache.init_app(app)

# simple app init -> no exceptions
def test_init_app(client_no_params, app):
    assert client_no_params.init_app(app) is None

def test_save_authorization_token(client_no_params):
    with pytest.raises(NotImplementedError):
        client_no_params.save_authorization_token({})


def test_get_authorization_token(client_no_params):
    with pytest.raises(NotImplementedError):
        client_no_params.get_authorization_token()

def test__fetch_token_not_implemented(client_no_params):
    with pytest.raises(NotImplementedError):
        client_no_params._fetch_token('test_state')


# creating a OAuth2 session
@pytest.mark.parametrize('test_token', [{'access_token' : 'test_token'}, None])
@pytest.mark.parametrize('test_state', ['test_state', None])
@pytest.mark.parametrize('test_scope', ['test_scope', None])
def test__make_session(client_no_params, test_token, test_state, test_scope, mocker):
    '''Создание сессии OAuth2
    Если переданы невалидные параметры (None):
        - генерируются новые ключ (token) и состояние (state)
    token - ключ авторизации
    state - состояние (используется для защиты от CSRF)
    scope - список желаемых разрешений
    '''
    test_gen_state = 'test_gen_state'
    test_gen_token = {'access_token' : 'test_gen_token'}
    mocker.patch('oauthlib.common.generate_token', return_value=test_gen_state)
    mocker.patch.object(DiscordOAuth2HttpClient,
                        'get_authorization_token', return_value=test_gen_token)

    session = client_no_params._make_session(
        test_token, test_state, test_scope)
    assert session.scope == test_scope
    assert session.token == test_token if test_token else test_gen_token
    assert session.state == test_state if test_state else test_gen_state


def test_request_unauthorized(client_no_params, mocker):
    '''Попытка отправки запроса по указанному маршруту от неавторизованного пользоваетеля
    Вызывает исключение Unauthorized
    '''
    args_sn = {'status_code': 401}
    mocker.patch.object(requests, 'request',
                        return_value=SimpleNamespace(**args_sn))
    with pytest.raises(Unauthorized):
        client_no_params.request(route='test_route', oauth=False)


def test_request_rate_limited(client_no_params, mocker):
    '''Попытка отправки запроса по указанному маршруту от пользоваетеля, которому ограничен
    доступ к API
    Вызывает исключение RateLimited с параметрами
    '''
    test_json = {'message': 'test_message',
                 'global': 'test', 'retry_after': '10000'}
    test_headers = {'user-agent': 'test/0.0.1'}
    args_sn = {'status_code': 429,
               'json': lambda: test_json, 'headers': test_headers}

    mocker.patch.object(requests, 'request',
                        return_value=SimpleNamespace(**args_sn))

    with pytest.raises(RateLimited) as ex_info:
        client_no_params.request(route='test_route', oauth=False)

    assert ex_info.value.message == test_json['message']
    assert ex_info.value.is_global == test_json["global"]
    assert ex_info.value.retry_after == test_json["retry_after"]
    assert ex_info.value.headers['user-agent'] == test_headers['user-agent']


def test_request(client_no_params, mocker):
    '''Обычный запрос
    Возвращает код 200 и вылидный ответ
    '''
    test_json = {'message': 'test_message',
                 'global': 'test', 'retry_after': '10000'}
    test_headers = {'user-agent': 'test/0.0.1'}
    args_sn = {'status_code': 200,
               'json': lambda: test_json, 'headers': test_headers}

    mocker.patch.object(requests, 'request',
                        return_value=SimpleNamespace(**args_sn))

    res_json = client_no_params.request(route='test_route', oauth=False)

    assert res_json['message'] == test_json['message']
    assert res_json['global'] == test_json["global"]
    assert res_json['retry_after'] == test_json["retry_after"]


def test_bot_request(client_no_params, mocker):
    '''
    Попытка HTTP запроса от имени бота
    '''
    test_json = {'message': 'test_message',
                 'global': 'test', 'retry_after': '10000'}
    args_sn = {'status_code': 200, 'json': lambda: test_json}

    mocker.patch.object(requests, 'request',
                        return_value=SimpleNamespace(**args_sn))

    res_json = client_no_params.bot_request(route='test_route')

    assert res_json['message'] == test_json['message']
    assert res_json['global'] == test_json["global"]
    assert res_json['retry_after'] == test_json["retry_after"]

def test_request_with_proxies(client_with_proxies, mocker):
    test_json = {'message': 'test_message',
                 'global': 'test', 'retry_after': '10000'}
    test_headers = {'user-agent': 'test/0.0.1'}
    args_sn = {'status_code': 200,
               'json': lambda: test_json, 'headers': test_headers}

    mocker.patch.object(requests, 'request',
                        return_value=SimpleNamespace(**args_sn))

    res_json = client_with_proxies.request(route='test_route', oauth=False)

    assert res_json['message'] == test_json['message']
    assert res_json['global'] == test_json["global"]
    assert res_json['retry_after'] == test_json["retry_after"]